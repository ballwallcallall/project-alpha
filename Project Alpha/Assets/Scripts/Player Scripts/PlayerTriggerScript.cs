﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerTriggerScript : MonoBehaviour {
    public delegate void TriggerEvent(bool Enter);
    public static event TriggerEvent nearNerd;

    [AdvancedInspector.Inspect(AdvancedInspector.InspectorLevel.Debug)]
    public bool OnChair = false;
    public Slider CopySlider;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Chair"))
        {
            OnChair = true;
            if(CopySlider.value >= 1.0f)
            {
                GameManager.instance.GameOver(true);
            }
        }
        else if(other.CompareTag("Teacher"))
        {
            GameManager.instance.GameOver(false);
            TeacherBehaviorManager.instance.CaughtPlayer();
        }
        else if(other.CompareTag("Nerd"))
        {
            nearNerd(true);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("Chair"))
        {
            OnChair = false;
        }
        else if(other.CompareTag("Nerd"))
        {
            nearNerd(false);
        }
    }
}
