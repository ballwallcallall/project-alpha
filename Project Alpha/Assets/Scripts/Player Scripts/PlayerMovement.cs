﻿using UnityEngine;
using System.Collections;
using AdvancedInspector;
using Lean;
using System.Collections.Generic;
using Pathfinding;
using System.Linq;
//This script and player trigger may need a rewrite to seperate logic.
public class PlayerMovement : TouchAction
{
    private Queue<Transform> ListOfPoints;
    private List<Transform> RegisteredPoints;

    public GameObject PathPointPrefab;

    public Transform dummyTargetPoint;

    PlayerTriggerScript playerTriggers;
    GameManager theManager;

    CustomAILerp PlayerAI;

    TouchAction touchAction;
    Dictionary<string, TouchAction> theDictionaryOfActions;

    Transform currentPoint;
    [AdvancedInspector.Inspect(InspectorLevel.Debug)]
    Transform notWaypointPoint;



    void Start()
    {
        RegisteredPoints = new List<Transform>();
        ListOfPoints = new Queue<Transform>();
        PlayerAI = GetComponent<CustomAILerp>();
        playerTriggers = GetComponent<PlayerTriggerScript>();
        theManager = GameManager.instance;
    }
    
    void OnEnable()
    {
        CustomAILerp.OnTargetReach += OnTargetReach;
    }

    void OnDisable()
    {
        CustomAILerp.OnTargetReach -= OnTargetReach;
    }

    void SetPoint(LeanFinger finger)
    {
        if (notWaypointPoint == null)
        {
            GameObject obj = Lean.LeanPool.Spawn(PathPointPrefab, Vector3.zero, Quaternion.identity);
            notWaypointPoint = obj.transform;
        }
        notWaypointPoint.position = finger.GetWorldPosition(9);
    }

    void OnTargetReach()
    {
        if (notWaypointPoint != null && notWaypointPoint.gameObject.activeSelf)
        {
            LeanPool.Despawn(notWaypointPoint.gameObject);
            notWaypointPoint = null;
        }
    }
    public override void OnFingerDown(LeanFinger finger)
    {
        SetPoint(finger);
    }
    public override void OnFingerDrag(Lean.LeanFinger finger)
    {
        SetPoint(finger);
    }

    public override void OnFingerUp(Lean.LeanFinger finger)
    {
        SetPoint(finger);
        dummyTargetPoint.position = notWaypointPoint.position;
        PlayerAI.SearchPath();
    }
}
