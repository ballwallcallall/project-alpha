﻿#define DOUBLE_TAP
using UnityEngine;
using System.Collections;
using Lean;
using AdvancedInspector;
public class PlayerInput : MonoBehaviour {
    public enum ConstantType
    {
        DoubleTap = 0,
        HoldTap,
        Toggle
    }
    [Inspect, Group("Constants")]
    public ConstantType constantType;


    TouchAction currentTouchAction;

    bool ShowTouchAction = false;
    [Inspect]
    void ShowAction()
    {
        ShowTouchAction = !ShowTouchAction;
    }

    bool showTouchFunction()
    {
        return ShowTouchAction;
    }
    [Inspect("showTouchFunction")]
    public TouchAction noAction;
    [Inspect("showTouchFunction")]
    public TouchAction moveAction;
    [Inspect("showTouchFunction")]
    public TouchAction copyAction;
    [Inspect("showTouchFunction")]
    public TouchAction throwAction;
    [Inspect("showTouchFunction")]
    public TouchAction multiAction;

    GameManager theManager;

    void Start()
    {
        theManager = GameManager.instance;
        wait = new WaitForSeconds(DoubleTapThreshold);
    }

    void OnEnable()
    {
        LeanTouch.OnFingerTap += OnFingerTap;
        LeanTouch.OnFingerDown += OnFingerDown;
        LeanTouch.OnFingerDrag += OnFingerDrag;
        LeanTouch.OnFingerUp += OnFingerUp;
        LeanTouch.OnMultiDrag += OnMultiDrag;
        LeanTouch.OnMultiTap += OnMultiTap;
        LeanTouch.OnFingerHeldSet += OnHeldSet;
        LeanTouch.OnFingerHeldDown += OnHeldDown;
        LeanTouch.OnFingerHeldUp += OnHeldUp;
    }
	
    void OnDisable()
    {
        LeanTouch.OnFingerTap -= OnFingerTap;
        LeanTouch.OnFingerDown -= OnFingerDown;
        LeanTouch.OnFingerDrag -= OnFingerDrag;
        LeanTouch.OnFingerUp -= OnFingerUp;
        LeanTouch.OnMultiDrag -= OnMultiDrag;
        LeanTouch.OnMultiTap -= OnMultiTap;
        LeanTouch.OnFingerHeldSet -= OnHeldSet;
        LeanTouch.OnFingerHeldDown -= OnHeldDown;
        LeanTouch.OnFingerHeldUp -= OnHeldUp;
    }

    void OnMultiTap(int fingerIndex)
    {
        //Deprecated
    }

    bool doubletap = false;

    IEnumerator isRunning;
    [Inspect("InspectItemDoubleTap")]
    public float DoubleTapThreshold = 0.2f;
    WaitForSeconds wait;
    LeanFinger theLastTap;
    IEnumerator DetectTap ()
    {
        isRunning = DetectTap();
        yield return wait;
        isRunning = null;
        if(doubletap)
        {
            ThrowItem(theLastTap);
        }
        else
        {
            MovePlayer(theLastTap);
        }
        doubletap = false;
    }

    void MovePlayer(LeanFinger finger)
    {
        if (currentTouchAction == noAction)
        {
            RaycastHit GroundHit;
            var ray = finger.GetRay();
            Physics.Raycast(ray, out GroundHit);
            if (GroundHit.collider != null && GroundHit.collider.CompareTag("Ground"))
            {
                currentTouchAction = moveAction;
            }
            currentTouchAction.OnFingerDown(finger);
            currentTouchAction.OnFingerUp(finger);
        }
    }

    void OnHeldSet(LeanFinger finger)
    {
        if (constantType == ConstantType.HoldTap)
        {
            if (theManager.AllowInput)
            {
                currentTouchAction.OnHeldSet(finger);
                //Update 
            }
        }
    }

    void OnHeldDown(LeanFinger finger)
    {
        currentTouchAction = throwAction;
        if (constantType == ConstantType.HoldTap)
        {
            if (theManager.AllowInput)
            {
                currentTouchAction.OnHeldDown(finger);
                //Set timer active to true;
                  //Start Timer
            }
        }
    }

    void OnHeldUp(LeanFinger finger)
    {
        if (constantType == ConstantType.HoldTap)
        {
            if (theManager.AllowInput)
            {
                currentTouchAction.OnHeldUp(finger);
                //Find timer end. Get force, fire projectile.
            }
        }
    }

    void ThrowItem(LeanFinger finger)
    {
        Debug.Log("Throwing Stuff");
    }

    private bool InspectItemDoubleTap()
    {
        if (constantType == PlayerInput.ConstantType.DoubleTap)
        {
            return true;
        }
        return false;
    }
    private bool InspectItemHoldTap()
    {
        if (constantType == PlayerInput.ConstantType.HoldTap)
        {
            return true;
        }
        return false;
    }

    private bool InspectItemToggle()
    {
        if (constantType == PlayerInput.ConstantType.Toggle)
        {
            return true;
        }
        return false;
    }
    [Inspect("InspectItemToggle")]
    bool ThrowStuff = false;

    void OnFingerTap(LeanFinger finger)
    {
        if (theManager.AllowInput)
        {
            switch (constantType)
            {
                case ConstantType.DoubleTap:
                    {
                        if (isRunning == null)
                        {
                            //Single Tap. Could be double Tap, so we wait a little
                            StartCoroutine(DetectTap());
                        }
                        else
                        {
                            doubletap = true;
                            //Double Tap
                        }
                        theLastTap = finger;
                        break;
                    }
                case ConstantType.Toggle:
                    {
                        if (ThrowStuff)
                        {
                            ThrowItem(finger);
                        }
                        else
                        {
                            MovePlayer(finger);
                        }
                        break;
                    }
                case ConstantType.HoldTap:
                    {
                        MovePlayer(finger);
                        break;
                    }
            }
        }
    }

    void OnFingerDown(LeanFinger finger)
    {
        if (LeanTouch.Fingers.Count == 1)
        {
            if (theManager.AllowInput)
            {
                Vector3 fingerPos = finger.GetWorldPosition(transform.position.z);
                Vector3 WorldPos = new Vector3(fingerPos.x, fingerPos.y, 0);
                var ray = finger.GetRay();
                RaycastHit2D hit = Physics2D.Raycast(fingerPos, Vector2.zero);
                if (hit.collider != null && hit.collider.name.Equals("FillBarButton"))
                {
                    //Copy Test
                    currentTouchAction = copyAction;
                }
                else
                {
                    currentTouchAction = noAction;
                }

                currentTouchAction.OnFingerDown(finger);
            }
        }
    }

    void OnFingerDrag(LeanFinger finger)
    {
        if(theManager.AllowInput)
        {
            currentTouchAction.OnFingerDrag(finger);
        }
    }

    void OnFingerUp(LeanFinger finger)
    {
        if (theManager.AllowInput)
        {
            currentTouchAction.OnFingerUp(finger);
        }
    }

    void OnMultiDrag(Vector2 finger)
    {
        if(theManager.AllowInput)
        {
           currentTouchAction = multiAction;
           currentTouchAction.OnMultiDrag(finger);
        }
    }
}
