﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {

    static PlayerManager instance;

    void Awake()
    {
        instance = this;
    }

    void OnDestroy()
    {
        instance = null;
    }
}
