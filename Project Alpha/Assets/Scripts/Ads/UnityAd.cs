﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class UnityAd : MonoBehaviour {

    void Start()
    {
        ShowAd();
    }
    public void ShowAd()
    {
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
            Debug.Log("Showing Ad");
        }
    }
}
