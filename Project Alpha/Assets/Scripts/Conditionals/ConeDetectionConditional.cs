﻿using UnityEngine;
using System.Collections;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;
public class ConeDetectionConditional : Conditional
{
    public FOV2DEyes FOV2DEyes;
    public string Tag = "Player";
    public List<RaycastHit2D> hits;

    public SharedGameObject detectedObject;
    public override void OnAwake()
    {

    }
    public override TaskStatus OnUpdate()
    {
        hits = FOV2DEyes.hits2D;

        for (int i = 0; i < hits.Count; i++)
        {
            if (hits[i].transform && (hits[i].transform.CompareTag(Tag) || hits[i].transform.CompareTag("Paper")))
            {
                detectedObject.Value = hits[i].transform.gameObject;
                return TaskStatus.Success;
            }
        }
        return TaskStatus.Failure;
    }

}
