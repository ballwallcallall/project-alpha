﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ThrowPaperAction : TouchAction
{
    public Image slider;
    public float currentPower;
    public GameObject Paper;
    public float LinearScaleForThrowing = 100;
    void Start()
    {

    }

    public override void OnHeldDown(Lean.LeanFinger finger)
    {

        slider.gameObject.SetActive(true);
        currentPower = 0;
        slider.fillAmount = currentPower;
       //Start timer
    }

    public override void OnHeldSet(Lean.LeanFinger finger)
    { 
        Debug.DrawLine(transform.position, finger.GetWorldPosition(9));
        if (currentPower < 1.0f)
        {
            currentPower += 1.0f * Time.deltaTime;
            slider.fillAmount = currentPower;
        }
        //update timer
    }

    public override void OnHeldUp(Lean.LeanFinger finger)
    {
        Vector2 finalDir = finger.GetWorldPosition(9) - transform.position;
        finalDir.Normalize();
        slider.gameObject.SetActive(false);

        GameObject newObj = Lean.LeanPool.Spawn(Paper, transform.position, Quaternion.identity);
        Rigidbody2D rg = newObj.GetComponent<Rigidbody2D>();
        rg.AddForce(finalDir * currentPower * LinearScaleForThrowing, ForceMode2D.Force);

    }


}
