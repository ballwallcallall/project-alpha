﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
public class MoveCamera : TouchAction
{
    Camera mainCamera;
    public float maxClampY = 10f;

    void Start()
    {
        mainCamera = Camera.main;
    }

    public override void OnMultiDrag(Vector2 delta)
    {
        if(Lean.LeanTouch.Fingers.Count >= 2)
        {
            Debug.Log("Delta is " + delta + " and amount of fingers active is " + Lean.LeanTouch.Fingers.Count);
            delta /= Lean.LeanTouch.Fingers.Count;
            delta *= -1;
            mainCamera.transform.DOLocalMoveY(Mathf.Clamp(delta.y, 0, maxClampY), 0.5f).SetEase(Ease.InOutQuad);
        }
    }
}
