﻿using UnityEngine;
using System.Collections;


public class TouchAction : MonoBehaviour
{
    public virtual void OnFingerDrag(Lean.LeanFinger finger)
    {

    }

    public virtual void OnFingerUp(Lean.LeanFinger finger)
    {

    }

    public virtual void OnFingerDown(Lean.LeanFinger finger)
    {

    }

    public virtual void OnMultiDrag(Vector2 delta)
    {

    }

    public virtual void OnHeldSet(Lean.LeanFinger finger)
    {

    }

    public virtual void OnHeldUp(Lean.LeanFinger finger)
    {

    }

    public virtual void OnHeldDown(Lean.LeanFinger finger)
    {

    }
}