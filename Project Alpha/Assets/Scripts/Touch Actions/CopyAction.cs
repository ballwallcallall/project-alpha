﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CopyAction : TouchAction
{
    public GameObject Pencil;
    public Slider CopySlider;
    Image FillSliderArea;

    [AdvancedInspector.Inspect(AdvancedInspector.InspectorLevel.Debug)]
    Color FillAreaOriginalColor;
    [AdvancedInspector.Inspect]
    public Color FinalColor;

    IEnumerator copyRoutine;

    bool NearNerd;

    void Start()
    {
        copyRoutine = StartCopy();
        FillSliderArea = CopySlider.fillRect.GetComponent<Image>();
        FillAreaOriginalColor = FillSliderArea.color;
        Pencil.SetActive(false);
        NearNerd = false;
    }

    void OnEnable()
    {
        PlayerTriggerScript.nearNerd += TriggerEventNearNerd;
    }

    void OnDisable()
    {
        PlayerTriggerScript.nearNerd -= TriggerEventNearNerd;
    }

    void TriggerEventNearNerd(bool Enter)
    {
        if(Enter)
        {
            NearNerd = true;
        }
        else
        {
            NearNerd = false;
            StopCoroutine(copyRoutine);
        }
    }

    public override void OnFingerDown(Lean.LeanFinger finer)
    {
        if(NearNerd)
            StartCoroutine(copyRoutine);
    }

    public override void OnFingerUp(Lean.LeanFinger finger)
    {
        StopCoroutine(copyRoutine);
    }

    IEnumerator StartCopy()
    {
        while (CopySlider.value < CopySlider.maxValue)
        {
            CopySlider.value += 1.0f * Time.deltaTime / 10.0f;
            FillSliderArea.color = Color.Lerp(FillAreaOriginalColor, FinalColor, CopySlider.value);
            yield return null;
        }
    }
}
